import { IRootState } from '../../types'
import { Actions } from './actions'

const initialState: IRootState = {
	posts: [],
}

const postReducer = (state = initialState, action: Actions) => {
	switch (action.type) {
		case 'SET_POSTS':
			return {
				...state,
				posts: action.payload,
			}
		case 'CREATE_POSTS': {
			return { ...state, posts: [action.payload] }
		}
		default:
			return state
	}
}

export default postReducer

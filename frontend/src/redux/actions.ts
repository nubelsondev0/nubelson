import api from '../services/api'
import { IPostProps } from '../../types'
import { Dispatch } from 'redux'

export interface SetPostsAction {
	type: 'SET_POSTS'
	payload: IPostProps[]
}

export interface GetPostsAction {
	type: 'GET_POSTS'
}

export interface CreatePostsAction {
	type: 'CREATE_POSTS'
	payload: IPostProps
}

export type Actions = SetPostsAction | GetPostsAction | CreatePostsAction

export const SetPosts = (posts: IPostProps[]): SetPostsAction => {
	return { type: 'SET_POSTS', payload: posts }
}

export const AddPost = (post: IPostProps): CreatePostsAction => {
	return { type: 'CREATE_POSTS', payload: post }
}

export const CreatePost = (post: IPostProps) => async (
	dispatch: Dispatch<CreatePostsAction>
) => {
	const newPost = await api.post('/posts', post)

	dispatch(AddPost(newPost.data))
}

export const getPosts = () => async (dispatch: Dispatch<SetPostsAction>) => {
	const response = await api.get('/posts')

	const posts = response.data
	dispatch(SetPosts(posts))
}

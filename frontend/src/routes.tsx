import React from 'react'
import { Switch, Route } from 'react-router-dom'

import Home from './pages/Home'
import Post from './pages/Post'
import NewPost from './pages/NewPost'

const Router: React.FC = () => {
	return (
		<Switch>
			<Route path='/' exact component={Home} />
			<Route path='/create' component={NewPost} />
			<Route path='/post/:postId' component={Post} />
		</Switch>
	)
}

export default Router

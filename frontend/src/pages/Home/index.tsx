import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import { ContentWrapper } from '../../components/Layout/elements'
import { IRootState } from '../../../types'
import { getPosts } from '../../redux/actions'
import styled from 'styled-components'

import { Cards } from '../../components/shared'

const Container = styled.div`
	width: 100%;
	height: 100%;
	display: flex;
	flex-direction: column-reverse;
	align-items: center;
	justify-content: flex-start;
`

const selectPosts = (state: IRootState) => state.posts

const Home: React.FC = () => {
	const posts = useSelector(selectPosts)
	const dispatch = useDispatch()

	useEffect(() => {
		dispatch(getPosts())
	}, [dispatch])

	return (
		<ContentWrapper>
			<Container>
				{posts.length ? (
					posts.map(post => (
						<Link
							to={`/post/${post.id}`}
							key={post.id}
							style={{ textDecoration: 'none' }}
						>
							<Cards.Post post={post} />
						</Link>
					))
				) : (
					<h1>You dont have any post to read. Sorry!</h1>
				)}
			</Container>
		</ContentWrapper>
	)
}

export default Home

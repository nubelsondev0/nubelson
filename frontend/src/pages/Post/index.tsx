import React, { useState, useEffect, useRef } from 'react'
import { useParams } from 'react-router-dom'
import styled from 'styled-components'
import { SubmitHandler, FormHandles } from '@unform/core'

import { Cards, Input, Buttons } from '../../components/shared'
import { ContentWrapper, StyledForm } from '../../components/Layout/elements'
import { IRouteParamProps, IPostProps, ICommentProps } from '../../../types'
import api from '../../services/api'

const defaultPost = {
	id: '8xf0y6ziyjabvozdd253nd',
	timestamp: 1467166872634,
	title: 'Udacity is the best place to learn React',
	body: 'Everyone says so after all.',
	author: 'thingtwo',
	category: 'react',
	voteScore: 6,
	deleted: false,
	commentCount: 2,
}

interface FormData {
	author: string
	body: string
}

const CommentWrapper = styled.div`
	width: 100%;
	max-width: 66rem;
	height: 100%;
	margin-top: 5rem;
	padding: 0 3rem;

	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: flex-start;
	gap: 5rem;

	@media ${props => props.theme.mediaQueries.small} {
	}
`

const CommentList = styled.div`
	width: 100%;
	height: 100%;

	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: flex-start;

	.commentListHeader {
		width: 100%;
		display: flex;
		align-items: center;
		justify-content: flex-end;
		margin-bottom: 1rem;

		p {
			font-size: 1.2rem;
			font-weight: 600;
		}
	}

	.commentGroup {
		width: 100%;
		height: 100%;
		display: flex;
		flex-direction: column-reverse;
		align-items: center;
		justify-content: flex-start;
	}
`

const Post: React.FC = () => {
	const formRef = useRef<FormHandles>(null)
	const [post, setPost] = useState<IPostProps>(defaultPost)
	const [comments, setComments] = useState<ICommentProps[]>()

	const { postId } = useParams<IRouteParamProps>()

	const getPostData = async (id: string) => {
		const response = await api.get(`posts/${id}`)
		setPost(response.data)
	}

	const getCommentData = async (id: string) => {
		const response = await api.get(`posts/${id}/comments`)

		setComments(response.data)
	}

	const handleSubmit: SubmitHandler<FormData> = (data, { reset }) => {
		console.log(data)

		reset()
	}

	useEffect(() => {
		getPostData(postId)
		getCommentData(postId)
	}, [postId, comments])

	return (
		<ContentWrapper>
			<Cards.Post post={post} detail />

			<CommentWrapper>
				<StyledForm onSubmit={handleSubmit} ref={formRef}>
					<Input.InputText
						name='author'
						label='Author'
						placeholder='Write your name here...'
					/>
					<Input.InputBody
						name='body'
						label='Body'
						placeholder='Write your comment here...'
					/>

					<Buttons.Text text='comment' type='submit' />
				</StyledForm>

				<CommentList>
					<div className='commentListHeader'>
						<p>{post.commentCount} comments</p>
					</div>

					<div className='commentGroup'>
						{comments?.length ? (
							comments.map(comment => (
								<Cards.Comment key={comment.id} comment={comment} />
							))
						) : (
							<p>This post dont have any comment.</p>
						)}
					</div>
				</CommentList>
			</CommentWrapper>
		</ContentWrapper>
	)
}

export default Post

import React, { useRef } from 'react'
import styled from 'styled-components'
import { SubmitHandler, FormHandles } from '@unform/core'
import { useDispatch } from 'react-redux'
import uuid from 'uuid'
import * as Yup from 'yup'
import { useHistory } from 'react-router-dom'

import { CreatePost } from '../../redux/actions'

import { IPostProps } from '../../../types'

import { ContentWrapper, StyledForm } from '../../components/Layout/elements'

import { Input, Buttons } from '../../components/shared'

const Wrapper = styled.div`
	width: 100%;
	display: flex;
	flex-direction: row;
	align-items: center;
	justify-content: space-between;

	@media ${props => props.theme.mediaQueries.small} {
		flex-direction: column;
	}
`

interface FormData {
	author: string
	title: string
	category: string
	body: string
}

const NewPost: React.FC = () => {
	const formRef = useRef<FormHandles>(null)

	const dispatch = useDispatch()
	const history = useHistory()

	const handleAddNewPost: SubmitHandler<FormData> = async (data, { reset }) => {
		try {
			const schema = Yup.object().shape({
				author: Yup.string().required(),
				title: Yup.string().min(6).required(),
				category: Yup.string().max(6).required(),
				body: Yup.string().max(70).required(),
			})

			await schema.validate(data, {
				abortEarly: false,
			})

			const newPost: IPostProps = {
				id: uuid.v4(),
				timestamp: Date.now(),
				title: data.title,
				body: data.body,
				author: data.author,
				category: data.category,
				voteScore: 0,
				deleted: false,
				commentCount: 0,
			}

			dispatch(CreatePost(newPost))

			reset()

			history.push('/')
		} catch (err) {
			if (err instanceof Yup.ValidationError) {
				// Validation failed
				alert('you need to fill the fields first!')
			}
		}
	}

	return (
		<ContentWrapper>
			<div>
				<StyledForm onSubmit={handleAddNewPost} ref={formRef}>
					<Input.InputText
						name='author'
						label='Author'
						placeholder='Your name'
					/>
					<Wrapper>
						<Input.InputText
							name='title'
							label='Title'
							placeholder='The best frontend library'
						/>
						<Input.InputText
							name='category'
							label='Category'
							placeholder='React'
						/>
					</Wrapper>
					<Input.InputBody
						name='body'
						label='Body'
						placeholder='Write your post...'
					/>

					<Buttons.Text text='post' type='submit' />
				</StyledForm>
			</div>
		</ContentWrapper>
	)
}

export default NewPost

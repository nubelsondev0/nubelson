import { DefaultTheme } from 'styled-components'
import mediaQueries from './mediaQueries'

const myTheme: DefaultTheme = {
	mediaQueries,
}

export { myTheme }

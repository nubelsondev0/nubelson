import { createGlobalStyle } from 'styled-components'

export default createGlobalStyle`
  *,
    *::before,
    *::after{
        margin: 0;
        padding: 0;
        box-sizing: inherit;
        outline: 0;
    }
    html{
        font-size: 62.5%;

        @media ${props => props.theme.mediaQueries.largest} {
            font-size: 60%;
        }
        @media ${props => props.theme.mediaQueries.large} {
            font-size: 57.5%;
        }
        @media ${props => props.theme.mediaQueries.small} {
            font-size: 55%;
        }
    }
    
    body{
        box-sizing: border-box;
        font-family: 'Merriweather', serif;
        font-weight: 400;
        line-height: 1.7;
        --white: #fff;
        --black: #000;
        --grey: #F9F9F9;
        background-color: var(--white);
        color: var(--text)
    }

    h1, h2, h3, h4, label, button{
      font-family: 'Archivo Narrow', sans-serif;
      text-transform: uppercase;
    }

    input{
      font-family: 'Merriweather', serif;
    }
`

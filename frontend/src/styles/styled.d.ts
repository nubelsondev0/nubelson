import 'styled-components'

// and extend them!
declare module 'styled-components' {
	export interface DefaultTheme {
		mediaQueries: {
			smallest: string
			smaller: string
			small: string
			medium: string
			large: string
			larger: string
			largest: string
		}
	}
}

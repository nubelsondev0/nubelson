import React from 'react'
import styled from 'styled-components'
import { Form } from '@unform/web'

const StyledContent = styled.div`
	width: 100%;
	max-width: 120rem;
	padding: 4rem;
	margin: 0 auto;
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: flex-start;

	@media ${props => props.theme.mediaQueries.small} {
		padding: 2rem;
	}
`

export const ContentWrapper = styled.div`
	width: 100%;
	max-width: 70rem;
	margin: 0 auto 10rem;
	height: 100%;

	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: flex-start;
`

export const StyledForm = styled(Form)`
	width: 100%;
	min-width: 66rem;
	min-height: 100%;
	padding: 0 3rem;

	display: flex;
	flex-direction: column;
	align-items: flex-start;
	justify-content: center;
	gap: 1rem;
	border-bottom: 4px solid var(--black);
	padding-bottom: 3rem;

	@media ${props => props.theme.mediaQueries.small} {
		min-width: 60rem;
	}

	@media ${props => props.theme.mediaQueries.smaller} {
		min-width: 50rem;
	}

	@media ${props => props.theme.mediaQueries.smallest} {
		min-width: 40rem;
	}
`

export const Content: React.FC = ({ children }) => {
	return <StyledContent>{children}</StyledContent>
}

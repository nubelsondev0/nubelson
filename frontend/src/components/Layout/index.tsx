import React from 'react'
import { ThemeProvider } from 'styled-components'

import GlobalStyles from '../../styles/global'
import { myTheme } from '../../styles/theme'

import { Content } from './elements'
import { Header } from '../shared'

const Layout: React.FC = ({ children }) => {
	return (
		<ThemeProvider theme={myTheme}>
			<Content>
				<Header.DesktopHeader />
				{children}
			</Content>
			<GlobalStyles />
		</ThemeProvider>
	)
}

export default Layout

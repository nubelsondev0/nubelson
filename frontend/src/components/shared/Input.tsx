import React, { useRef, useEffect } from 'react'
import { useField } from '@unform/core'
import styled from 'styled-components'

interface Props {
	name: string
	label?: string
}

interface InputStyledProps {
	full?: boolean
}

const StyledInput = styled.div<InputStyledProps>`
	display: flex;
	flex-direction: column;
	align-items: flex-start;
	justify-content: center;
	gap: 0.5rem;

	label {
		font-size: 1.4rem;
		font-weight: 600;
	}

	input,
	textarea {
		min-width: 29rem;
		min-height: 5rem;
		background-color: var(--grey);
		border: none;
		padding: 2rem;
		font-weight: 300;

		@media ${props => props.theme.mediaQueries.small} {
			min-width: 54rem;
		}

		@media ${props => props.theme.mediaQueries.smaller} {
			min-width: 44rem;
		}

		@media ${props => props.theme.mediaQueries.smallest} {
			min-width: 34rem;
		}

		&::placeholder {
			color: #bebebe;
			font-size: 1.2rem;
			font-style: italic;
		}
	}

	textarea {
		min-width: 60rem;

		@media ${props => props.theme.mediaQueries.small} {
			min-width: 54rem;
		}

		@media ${props => props.theme.mediaQueries.smaller} {
			min-width: 44rem;
		}

		@media ${props => props.theme.mediaQueries.smallest} {
			min-width: 34rem;
		}
	}
`

type InputProps = JSX.IntrinsicElements['input'] & Props

const InputText: React.FC<InputProps> = ({ name, label, ...rest }) => {
	const inputRef = useRef<HTMLInputElement>(null)

	const { fieldName, defaultValue, registerField } = useField(name)

	useEffect(() => {
		registerField({
			name: fieldName,
			path: 'value',
			ref: inputRef.current,
		})
	}, [fieldName, registerField])

	return (
		<StyledInput>
			<label htmlFor={fieldName}>{label}</label>
			<input
				id={fieldName}
				ref={inputRef}
				defaultValue={defaultValue}
				{...rest}
			/>
		</StyledInput>
	)
}

type TextAreaProps = JSX.IntrinsicElements['textarea'] & Props

const InputBody: React.FC<TextAreaProps> = ({ name, label, ...rest }) => {
	const textAreaRef = useRef<HTMLTextAreaElement>(null)

	const { fieldName, defaultValue, registerField } = useField(name)

	useEffect(() => {
		registerField({
			name: fieldName,
			path: 'value',
			ref: textAreaRef.current,
		})
	}, [fieldName, registerField])

	return (
		<StyledInput>
			<label htmlFor={fieldName}>{label}</label>
			<textarea
				id={fieldName}
				ref={textAreaRef}
				defaultValue={defaultValue}
				{...rest}
			/>
		</StyledInput>
	)
}

const Input = {
	InputText,
	InputBody,
}

export default Input

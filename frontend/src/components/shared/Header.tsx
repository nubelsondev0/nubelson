import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { RiHomeLine } from 'react-icons/ri'

import Button from './Buttons'

const StyledHeader = styled.div`
	width: 100%;
	margin-bottom: 10rem;

	display: flex;
	flex-direction: row;
	align-items: center;
	justify-content: space-between;
`

const StyledHomeIcon = styled(RiHomeLine)`
	width: 3.5rem;
	height: 3.5rem;
	font-size: 3.5rem;
	color: var(--black);
`

const DesktopHeader: React.FC = () => {
	return (
		<StyledHeader>
			<Link to='/'>
				<StyledHomeIcon />
			</Link>
			<Link to='/create'>
				<Button.Icon />
			</Link>
		</StyledHeader>
	)
}

const Header = {
	DesktopHeader,
}

export default Header

import React from 'react'
import styled from 'styled-components'
import { IPostProps, ICommentProps } from '../../../types'
import moment from 'moment'
import { IconContext } from 'react-icons'
import {
	RiDeleteBin2Line,
	RiThumbUpLine,
	RiThumbDownLine,
	RiEditLine,
} from 'react-icons/ri'

interface IPost {
	post: IPostProps
	detail?: boolean
}

interface IComment {
	comment: ICommentProps
}

interface PostStyledProps {
	detail?: boolean
}

const StyledCard = styled.div<PostStyledProps>`
	min-width: 66rem;
	height: 100%;
	padding: 3rem;
	outline: 4px solid transparent;
	cursor: ${props => !props.detail && 'pointer'};
	color: var(--black);

	${props =>
		!props.detail &&
		`
		&:hover {
			outline: 4px solid var(--black);
			transition: 0.2s;
			}
		`}

	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
	gap: 1rem;

	@media ${props => props.theme.mediaQueries.small} {
		min-width: 60rem;
	}

	@media ${props => props.theme.mediaQueries.smaller} {
		min-width: 50rem;
	}

	@media ${props => props.theme.mediaQueries.smallest} {
		min-width: 40rem;
	}

	.postHeader,
	.postFooter {
		width: 100%;

		display: flex;
		flex-direction: row;
		align-items: center;
		justify-content: space-between;

		${props =>
			props.detail &&
			`
			.commentCount{
				display: none;
			}
		`}

		p {
			font-size: 1.2rem;
			font-style: italic;
			font-weight: 200;
			text-decoration: none;

			span {
				font-weight: bold;

				&.categoryName {
					text-decoration: underline;
					text-transform: capitalize;
				}
			}
		}
	}

	.postBody {
		width: 100%;

		display: flex;
		flex-direction: column;
		align-items: flex-start;
		justify-content: center;

		h2 {
			font-size: 2.6rem;
			font-weight: 700;
		}

		p {
			font-weight: 200;
			font-size: 1.4rem;
		}

		@media ${props => props.theme.mediaQueries.small} {
			h2 {
				font-size: 2.3rem;
			}
		}

		@media ${props => props.theme.mediaQueries.smaller} {
		}
	}
`

const StyledComment = styled.div`
	width: 100%;
	height: 100%;
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: flex-start;
	padding: 2rem;

	.commentHeader {
		width: 100%;
		height: 100%;
		margin-bottom: 1rem;
		display: flex;
		flex-direction: row;
		align-items: center;
		justify-content: space-between;

		h3 {
			font-size: 1.2rem;
			font-weight: 600;
		}

		p {
			font-size: 1rem;
			font-style: italic;
			font-weight: 200;
			span {
				font-weight: 700;
			}
		}
	}

	.commentBody {
		width: 100%;
		height: 100%;
		margin-bottom: 2rem;
		display: flex;
		flex-direction: row;
		align-items: center;
		justify-content: flex-start;
		font-size: 1rem;
		font-weight: 400;
	}

	.commentFooter {
		width: 100%;
		height: 100%;
		display: flex;
		flex-direction: row;
		align-items: center;
		justify-content: space-between;

		.commentVoting,
		.commentManage {
			display: flex;
			flex-direction: row;
			align-items: center;
			justify-content: center;
			gap: 0.5rem;

			p {
				font-size: 1rem;
				font-weight: 400;
			}
		}
	}
`

const formatDate = (dateUnix: number): string => {
	return moment(dateUnix).format('MMM DD, Y')
}

const Post: React.FC<IPost> = ({ post, detail }) => {
	return (
		<StyledCard detail={detail}>
			<div className='postHeader'>
				<p>
					On <span className='categoryName'>{post.category}</span>
				</p>

				<p className='commentCount'>
					<span>{post.commentCount} comments</span>
				</p>
			</div>

			<div className='postBody'>
				<h2>{post.title}</h2>
				<p>{post.body}</p>
			</div>

			<div className='postFooter'>
				<p>
					At <span>{formatDate(post.timestamp)}</span>
				</p>
				<p>
					By <span>{post.author}</span>
				</p>
			</div>
		</StyledCard>
	)
}

const Comment: React.FC<IComment> = ({ children, comment }) => {
	return (
		<StyledComment>
			<div className='commentHeader'>
				<h3>{comment.author}</h3>
				<p>
					At <span>{formatDate(comment.timestamp)}</span>
				</p>
			</div>
			<p className='commentBody'>{comment.body}</p>

			<div className='commentFooter'>
				<IconContext.Provider value={{ size: '15' }}>
					<div className='commentVoting'>
						<RiThumbUpLine />
						<p>{comment.voteScore}</p>
						<RiThumbDownLine />
					</div>

					<div className='commentManage'>
						<RiEditLine />
						<RiDeleteBin2Line />
					</div>
				</IconContext.Provider>
			</div>
		</StyledComment>
	)
}

const Cards = {
	Post,
	Comment,
}

export default Cards

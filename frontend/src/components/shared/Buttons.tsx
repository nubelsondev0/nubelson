import React from 'react'
import styled from 'styled-components'
import { RiEditBoxLine } from 'react-icons/ri'

interface ITextButtonProps {
	text: string
}

type ButtonProps = JSX.IntrinsicElements['button'] & ITextButtonProps

const StyledIconButton = styled.div`
	width: 3.5rem;
	height: 3.5rem;
	padding: 0.3rem;
	background-color: var(--black);

	display: flex;
	justify-content: center;
	align-items: center;

	& > * {
		color: var(--white);
	}

	@media ${props => props.theme.mediaQueries.small} {
		width: 3rem;
		height: 3rem;
	}
`

const StyledTextButton = styled.button`
	align-self: flex-end;
	margin-top: 1rem;
	height: 5rem;
	width: 12rem;
	padding: 1.5rem;
	background-color: var(--black);
	border: none;
	color: var(--white);
	font-size: 1.4rem;
	font-weight: 600;
	cursor: pointer;

	@media ${props => props.theme.mediaQueries.small} {
		width: 9rem;
		height: 4.5rem;
		font-size: 1.2rem;
	}
`

const Icon: React.FC = () => {
	return (
		<StyledIconButton>
			<RiEditBoxLine size='24' />
		</StyledIconButton>
	)
}

const Text: React.FC<ButtonProps> = ({ text }) => {
	return <StyledTextButton> {text} </StyledTextButton>
}

const Buttons = {
	Icon,
	Text,
}

export default Buttons

import axios from 'axios'

const api = axios.create({
	baseURL: 'http://localhost:3001',
})

api.defaults.headers.common['Authorization'] = process.env.REACT_APP_API_AUTH

export default api

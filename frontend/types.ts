export interface IPostProps {
	id: string
	timestamp: number
	title: string
	body: string
	author: string
	category: string
	voteScore: number
	deleted: boolean
	commentCount: number
}

export interface ICommentProps {
	id: string
	timestamp: number
	parentId: string
	title: string
	body: string
	author: string
	category: string
	voteScore: number
	deleted: boolean
	parentDeleted: boolean
}

export interface IRootState {
	posts: IPostProps[]
}

export interface IRouteParamProps {
	postId: string
}
